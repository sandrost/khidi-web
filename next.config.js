/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/',
        destination: '/home',
      },
    ];
  },
  notPage: ['components'],
  images: { domains: ['127.0.0.1'] },
};

module.exports = nextConfig;
