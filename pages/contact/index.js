import { Field, Form, Formik } from 'formik';
import styles from './Contact.module.scss';
import { React, useRef } from 'react';
import * as Yup from 'yup';
import logo from '../../assets/khidilogo.png';
import discord from '../../assets/footer/discord.png';
import instagram from '../../assets/footer/instagram.png';
import telegram from '../../assets/footer/telegram.png';
import twitter from '../../assets/footer/twitter.png';

const mailForm = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  lastName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
});

const Contact = () => {
  const inputFile = useRef();

  const fileinput = (e) => {
    inputFile.current.click();
  };

  return (
    <div className={styles.contact}>
      <div className={styles.contactform}>
        <h2>Contact form</h2>
        <p>
          understanding and outlook. It also deals with the switching of
          orientation
        </p>
        <Formik
          initialValues={{
            subject: '',
            bodytext: '',
            email: '',
          }}
          validationSchema={mailForm}
          onSubmit={(values) => {}}
        >
          {({ errors, touched }) => (
            <Form>
              <Field name="subject" placeholder={'subject'} />
              {errors.subject && touched.subject ? (
                <div>{errors.subject}</div>
              ) : null}
              <Field name="email" type="email" placeholder={'email'} />
              {errors.email && touched.email ? <div>{errors.email}</div> : null}
              <Field name="bodytext" placeholder={'body text'} as="textarea" />
              {errors.bodytext && touched.bodytext ? (
                <div>{errors.bodytext}</div>
              ) : null}

              <div className={styles.formfooter}>
                <button type="submit">Send</button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <div className={styles.map}>
        <div>
          <img src={logo.src}></img>
          <span>khidi</span>
        </div>
      </div>
      <div className={styles.contactinfo}>
        <div className={styles.header}>
          <span>
            Tbilisi <span>Georgia</span>
          </span>
          <p>vakhushti bagrationi bridge N 123</p>
        </div>
        <div className={styles.footer}>
          <div>
            <span className={styles.mail}>Info@khidi.com</span>
            <span className={styles.number}>+995 (95) 594523</span>
          </div>
          <ul>
            <li>
              <a href="#">
                <img src={twitter.src}></img>
              </a>
            </li>
            <li>
              <a href="#">
                <img src={telegram.src}></img>
              </a>
            </li>
            <li>
              <a href="#">
                <img src={instagram.src}></img>
              </a>
            </li>
            <li>
              <a href="#">
                <img src={discord.src}></img>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className={styles.googlemap}>
        <a href="#">google map</a>
      </div>
    </div>
  );
};

export default Contact;
