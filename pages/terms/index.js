import React from "react";
import styles from "./Terms.module.scss";

const Terms = () => {
  return (
    <div className={styles.terms}>
      <div className={styles.title}>
        <h2>TERMS & CONDITIONS</h2>
      </div>
      <div className={styles.content}>
        <div className={styles.scrollcontent}>
          <p>
            ‘Five Space Square’ is in a certain sense a pragmatic
            systematization of the surrounding environment based on the
            difference between sand and stone, going from small to large, framed
            by pre-rusted corten steel. It is analytical in nature, as my work
            often is, and by decomposing the earth that is walked upon, it gives
            both understanding and outlook. It also deals with the switching of
            orientation; what is in nature vertically is presented horizontally,
            thereby creating a liminal space.
          </p>
          <p>
            ‘Five Space Square’ is in a certain sense a pragmatic
            systematization of the surrounding environment based on the
            difference between sand and stone, going from small to large, framed
            by pre-rusted corten steel. It is analytical in nature, as my work
            often is, and by decomposing the earth that is walked upon, it gives
            both understanding and outlook. It also deals with the switching of
            orientation; what is in nature vertically is presented horizontally,
            thereby creating a liminal space.
          </p>
          <p>
            ‘Five Space Square’ is in a certain sense a pragmatic
            systematization of the surrounding environment based on the
            difference between sand and stone, going from small to large, framed
            by pre-rusted corten steel. It is analytical in nature, as my work
            often is, and by decomposing the earth that is walked upon, it gives
            both understanding and outlook. It also deals with the switching of
            orientation; what is in nature vertically is presented horizontally,
            thereby creating a liminal space.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Terms;
