import { useEffect, useState } from 'react';
import axios from 'axios';

import { Event, Grid, InfoBlock, Landing } from '../../shared';
import { API } from '../../config';

import styles from './home.module.scss';

export async function getServerSideProps() {
  let events = [];

  await axios
    .get(`${API}/event`)
    .then((res) => (events = res.data?.data))
    .catch(({ response }) =>
      console.error(response.status, response.statusText)
    );

  return {
    props: { events },
  };
}

const main = [
  {
    size: 2,
    component: <Landing />,
  },
  {
    size: 1,
    component: <InfoBlock />,
  },
];

export default function Home({ events }) {
  const [data, setData] = useState(main);

  useEffect(() => {
    const newData = [...main];

    events.forEach((event) =>
      newData.push({ size: 1, component: <Event data={event} /> })
    );

    setData(newData);
  }, [events]);

  return (
    <div className={styles.homepage} id={'container'}>
      <Grid data={data} />
    </div>
  );
}
