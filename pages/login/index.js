import React from "react";
import FormLayout from "../../shared/layout/form/FormLayout";
import * as yup from "yup";

const fields = [
  {
    name: "email",
    type: "text",
    label: "Email",
  },
  { name: "password", type: "password", label: "Password" },
];

const initialValues = {
  email: "",
  password: "",
  check: "",
};
const schema = {
  email: yup
    .string("Enter your email")
    .email("Enter a valid email")
    .required("Email"),
  password: yup.string("Enter your password").required("Password"),
};

const Login = () => {
  return (
    <FormLayout
      fields={fields}
      initialValues={initialValues}
      schema={schema}
      submitText={"login"}
      title={"log in page"}
      style={"login"}
      login={true}
      show={true}
    ></FormLayout>
  );
};

export default Login;
