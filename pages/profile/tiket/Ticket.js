import React, { useState } from "react";
import styles from "./Ticket.module.scss";
import rightarrow from "../../../assets/rightarrow.svg";
import qrcode from "../../../assets/qrcode.png";
import ticketcover from "../../../assets/ticketcover.jpg";
import closeIcon from "../../../assets/x.svg";

const Tiket = ({ event }) => {
  const [click, setClicked] = useState(true);

  return (
    <div className={styles.ticket}>
      <div className={styles.header}>
        <h2>ticket</h2>
        <button onClick={() => event(false)}>
          <img src={closeIcon.src}></img>
        </button>
      </div>
      <div className={styles.tickedate}>
        <img src={ticketcover.src}></img>
        <div className={styles.about}>
          <h2>Club nights</h2>
          <span>FRIDAY 中 25 MARCH</span>
          <span>CLUB NIGHTS 2022 & upcoming</span>
        </div>
      </div>
      <div className={styles.mobileticket}>
        <img src={ticketcover.src}></img>
        {!click ? (
          <div className={styles.userinfo}>
            <div>
              <p>ilia chanchaleisvhli</p>
              <span className={styles.date}>21.07.2022</span>
              <div>
                <span className={styles.pricelabel}>PRice</span>
                <span className={styles.price}>40 Gel</span>
              </div>
            </div>
            <hr />
            <img src={qrcode.src} className={styles.qrcode}></img>
          </div>
        ) : (
          <div onClick={() => setClicked(!click)} className={styles.about}>
            <h2>Club nights</h2>
            <span>FRIDAY 中 25 MARCH</span>
            <span>CLUB NIGHTS 2022 & upcoming</span>
          </div>
        )}
      </div>
      <div className={styles.tiketprice}>
        <div className={styles.userinfo}>
          {click && (
            <>
              <p>ilia chanchaleisvhli</p>
              <span className={styles.date}>21.07.2022</span>
            </>
          )}

          <div>
            <span className={styles.pricelabel}>PRice</span>

            {click ? (
              <span className={styles.price}>40 Gel</span>
            ) : (
              <span className={styles.priceborder}>40 Gel</span>
            )}
          </div>
        </div>
        <button
          className={styles.ticketbutton}
          onClick={() => setClicked(true)}
        >
          {click ? (
            <img src={qrcode.src} className={styles.qrcode}></img>
          ) : (
            <img src={rightarrow.src} className={styles.arrow}></img>
          )}
        </button>
      </div>
    </div>
  );
};

export default Tiket;
