import React, { useState } from "react";
import { Formik, Form, Field } from "formik";
import styles from "./Editprofile.module.scss";
import * as Yup from "yup";
import Profile from "../profile/Profile";
import closeIcon from "../../../assets/x.svg";

import Tiket from "../tiket/Ticket";

const SignupSchema = Yup.object().shape({
  firstName: Yup.string().min(2, "Too Short!").max(50, "Too Long!"),
  lastName: Yup.string().min(2, "Too Short!").max(50, "Too Long!"),
  email: Yup.string().email("Invalid email"),
});

const fields = [
  {
    name: "name",
    type: "text",
    label: "Name",
  },
  {
    name: "surname",
    type: "text",
    label: "Surname",
  },
  {
    name: "email",
    type: "email",
    label: "Email",
  },
  {
    name: "username",
    type: "text",
    label: "Username",
  },
  {
    name: "social",
    type: "text",
    label: "Social",
  },
  {
    name: "birthdate",
    type: "text",
    label: "BirthDate",
  },
  {
    name: "status",
    type: "text",
    label: "Status",
  },
  {
    name: "mobile",
    type: "number",
    label: "Mobile",
  },
];

const Editprofile = () => {
  const [data, setData] = useState({
    name: "Ilia",
    surname: "Chanchaleishvili",
    email: "Ilia.Chanchaleishvili@Gmail.Com",
    username: "Ilo Chani",
    social: "Ilochani",
    birthdate: "21.07.1987",
    status: "Autorized Profile",
    mobile: "595 505354",
  });

  const [close, setClose] = useState(false);
  const [event, closeEvent] = useState(true);

  return (
    <Profile data={data} close={setClose} event={closeEvent}>
      {close ? (
        <div className={styles.Editprofile}>
          <div className={styles.close}>
            <button onClick={() => setClose(false)}>
              <img src={closeIcon.src}></img>
              <span>close</span>
            </button>
          </div>
          <div className={styles.header}>
            <h2>update profile</h2>
            <button onClick={() => setClose(false)}>
              <img src={closeIcon.src}></img>
            </button>
            <p>
              understanding and outlook. It also deals with the switching of
              orientation
            </p>
          </div>
          <Formik
            initialValues={{
              name: "",
              surname: "",
              email: "",
              username: "",
              social: "",
              birthdate: "",
              status: "",
              mobile: "",
            }}
            validationSchema={SignupSchema}
            onSubmit={(values) => {
              setData(values);
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <div className={styles.inputwrapper}>
                  {fields.map((item, key) => (
                    <Field
                      key={key}
                      name={item.name}
                      className={styles.inputs}
                      placeholder={item.label}
                    />
                  ))}
                  {errors.firstName && touched.firstName ? (
                    <div>{errors.firstName}</div>
                  ) : null}
                </div>
                <button type="submit">update profile</button>
              </Form>
            )}
          </Formik>
        </div>
      ) : (
        event && <Tiket event={closeEvent} />
      )}
    </Profile>
  );
};

export default Editprofile;
