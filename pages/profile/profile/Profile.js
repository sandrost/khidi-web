import { React } from 'react';
import styles from './Profile.module.scss';
import logo from '../../../assets/blackLogo.png';

const Profile = ({ children, data = {}, close, event }) => {
  return (
    <>
      <div className={styles.wrapper}>
        <div className={styles.profile}>
          <div className={styles.header}>
            <a>
              <img src={logo.src}></img>
            </a>
            <h2>khidi profile</h2>
          </div>
          <div className={styles.title}>
            <div>
              <p>ilia chanchaleishvili</p>
              <div>
                <span>Verified</span>
                <span className={styles.icon}></span>
              </div>
            </div>
          </div>
          <div className={styles.userinfo}>
            <ul>
              <li>Name : {data.name} </li>
              <li>Surname : {data.surname}</li>
              <li>email : {data.email}</li>
              <li>username : {data.username}</li>
              <li>
                fb profile :<a> {data.social}</a>
              </li>
              <li>birth date : {data.birthdate}</li>
              <li>status: {data.status}</li>
              <li>
                <a>mobile N : {data.mobile}</a>
              </li>
            </ul>
          </div>
          <div className={styles.editprofile}>
            <button onClick={() => close(true)}>
              edit profile
              <span className={styles.editicon}></span>
            </button>
          </div>
        </div>
        <button className={styles.mobilebtn} onClick={() => event(true)}>
          Ticket
        </button>
      </div>

      {children}
    </>
  );
};

export default Profile;
