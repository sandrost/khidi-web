
import { useState, useEffect } from "react"
import { useDrag } from '@use-gesture/react'
import { a, useSpring, config } from '@react-spring/web'


import styles from './cart.module.scss'
// const items = ['save item', 'open item', 'share item', 'delete item', 'cancel']
const height = 305
export default function Cart() {

    const [{ y }, api] = useSpring(() => ({ y: height }))

    const open = ({ canceled }) => {
        // when cancel is true, it means that the user passed the upwards threshold
        // so we change the spring config to create a nice wobbly effect
        api.start({ y: 0, immediate: false, config: canceled ? config.gentle : config.stiff })
    }
    const close = (velocity = 0) => {
        api.start({ y: height, immediate: false, config: { ...config.stiff, velocity } })
    }

    const bind = useDrag(
        ({
            last,
            velocity: [, vy],
            direction: [, dy],
            movement: [, my],
            cancel,
            canceled
        }) => {
            // if the user drags up passed a threshold, then we cancel
            // the drag so that the sheet resets to its open position
            if (my < -10) cancel();

            // when the user releases the sheet, we check whether it passed
            // the threshold for it to close, or if we reset it to its open positino
            if (last) {
                my > height * 0.5 || (vy > 0.5 && dy > 0)
                    ? close(vy)
                    : open({ canceled });
            }
            // when the user keeps dragging, we just move the sheet according to
            // the cursor position
            else api.start({ y: my, immediate: false });
        },
        {
            from: () => [0, y.get()],
            filterTaps: true,
            bounds: { top: 0 },
            rubberband: true
        }
    );


    const display = y.to((py) => (py < height ? '' : ''))

    const [text, setText] = useState('')
    const [data, setData] = useState([])
    const [ticket, setTicket] = useState([{
        id: 1, title: 'Club nights', subText: 'FRIDAY 中 25 MARCH\n CLUB NIGHTS 2022 & upcoming', price: '40'
    }])
    const handleFeedback = (e) => {
        setText(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setData([...data, text]);
        setText("")
    }

    const deleteItem = (item) => {
        setData(data.filter((el) => el !== item))
    }

    // useEffect(() => {
    //     console.log(data.length)
    // }, [data]);
    return (
        <div className={styles.cart}>
            <section>
                {
                    ticket.length > 0 &&
                    ticket.map((item, key) => {
                        return (
                            <div key={key} className={styles.ticket}>
                                <div className={styles.container}>

                                    <div className={styles.img} >

                                    </div>
                                    <div>
                                        <h2>{item.title}</h2>
                                        <p>{item.subText}</p>
                                    </div>
                                </div>
                                <div className={styles.amountWrapper}>
                                    <div className={data.length > 0 ? styles.priceDeducted : styles.priceE}>
                                        <hr style={{ opacity: data.length > 0 ? 1 : 0 }} />
                                        <p className={styles.priceText}>Price</p>
                                        <span></span>
                                        <p className={styles.priceAmount}>40 GEL</p>
                                    </div>
                                    {
                                        data.length > 0 &&
                                        <div className={styles.Newprice}>
                                            <p className={styles.NewpriceText}>Price</p>
                                            <span></span>
                                            <p className={styles.NewpriceAmount}>20 GEL</p>
                                        </div>
                                    }
                                </div>
                            </div>

                        )
                    })



                }

                <div className={styles.empty}>

                </div>
            </section >
            <section>
                <div className={styles.wrapper}>
                    <div>

                        <div className={styles.logo}></div>
                        <span className={styles.title}>khidi</span>
                        {/* <h2 className={styles.final}>final page</h2> */}
                        {/* <p className={styles.headerText}>understanding and outlook. It also deals with the switching of orientation */}
                        {/* </p> */}
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className={styles.promoArray} style={{ marginTop: data.length > 0 && '30px' }}>
                            {
                                data && data.map((item, key) => {
                                    return (

                                        item && <div key={key} className={styles.item}>
                                            <p>Promo Code: <span>{item}</span></p>
                                            <button onClick={(e) => deleteItem(item)}></button>

                                        </div>


                                    )
                                })

                            }
                        </div>
                        <div className={styles.codeWrapper}>

                            <input placeholder='promo code' disabled={data.length === ticket.length} value={text} onChange={handleFeedback} onKeyPress={(e) => e.key === 'Enter' && handleSubmit(e)} />
                            <button type={"submit"}>Apply<span></span></button>
                        </div>
                    </form>
                    <div className={styles.price}>
                        <p>Total Price</p>
                        <p>40 GEL</p>
                    </div>
                    <div className={styles.buytBtn}>

                        <button>checkout</button>
                    </div>
                </div>
            </section >
            {/* <div className={styles.actionBtn} onClick={open} /> */}
            <a.div
                className={styles.sheet}
                {...bind()}
                style={{ display, bottom: `calc(-100vh + ${height + 115}px)`, y }}
            >
                <>
                    <hr />

                    <div className={styles.cartModal}

                    >
                        <div className={styles.cartModalHeader}>

                            <div className={styles.logo}></div>
                            <span className={styles.title}>khidi</span>
                            <h2 className={styles.final}>final page</h2>
                            <p className={styles.headerText}>understanding and outlook. It also deals with the switching of orientation
                            </p>
                            <form onSubmit={handleSubmit}>
                                <div className={styles.promoArray} style={{ marginTop: data.length > 0 && '30px' }}>
                                    {
                                        data && data.map((item, key) => {
                                            return (

                                                item && <div key={key} className={styles.item}>
                                                    <p>Promo Code: <span>{item}</span></p>
                                                    <button onClick={(e) => deleteItem(item)}></button>

                                                </div>


                                            )
                                        })

                                    }
                                </div>
                                <div className={styles.codeWrapper}>

                                    <input placeholder='promo code' disabled={data.length === ticket.length} value={text} onChange={handleFeedback} onKeyPress={(e) => e.key === 'Enter' && handleSubmit(e)} />
                                    <button type={"submit"}>Apply<span></span></button>
                                </div>
                            </form>
                            <div className={styles.price}>
                                <p> Price 40 GEL</p>
                            </div>
                        </div>
                        <div className={styles.cartModalFooter}>
                            <p>

                                checkout
                            </p>
                        </div>
                    </div>
                </>
            </a.div>

            {/* <div className={`${styles["Modal"]} ${styles[displayModal ? "Show" : ""]}`}></div > */}

        </div >
    )
}



