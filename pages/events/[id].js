import { useRouter } from 'next/router';
import moment from 'moment';

import styles from './Events.module.scss';
// import banner from '../../assets/banner.png';

// export const getStaticPaths = async () => {
//   const res = await fetch('http://46.101.103.162/api/event');
//   const result = await res.json();

//   const paths = result.data.map((event) => ({
//     params: { id: event.id.toString() },
//   }));

//   return {
//     paths,
//     fallback: false,
//   };
// };

// export const getStaticProps = async ({ params }) => {
//   const res = await fetch(`http://46.101.103.162/api/event`);
//   const result = await res.json();

//   const eventData = result.data.filter(
//     (item) => item.id.toString() == params.id
//   );

//   return {
//     props: {
//       event: eventData[0],
//     },
//   };
// };

export default function EventPage({ event = {} }) {
  var today = moment(event.start_date);
  var date = {
    day: today.format('dddd'),
    month: today.format('MMM'),
    num: today.format('DD').replace(/^0(?:0:0?)?/, ''),
  };

  return (
    <div className={styles.eventDetails}>
      <section>
        <div>
          <div
            style={
              {
                // backgroundImage: event.banner
                //   ? `url(${event.banner.src})`
                //   : `url(${banner.src})`,
              }
            }
            className={styles.img}
          ></div>
        </div>
        <div className={styles.details}>
          <hr />
          <p className={styles.text}>
            {' '}
            understanding and outlook. It also deals with the switching of
            orientation
          </p>
        </div>
      </section>
      <section>
        <div className={styles.textStyles}>
          <p className={styles.title}>{event.title}</p>
          <div className={styles.price}>
            <p>PRice</p>
            <span></span>
            <p>{event.price ? event.price : '40'} Gel</p>
          </div>
        </div>
        <div className={styles.textStyles}>
          <h2>CLUB NIGHTS</h2>
          <div className={styles.price} style={{ paddingTop: '6px' }}>
            <p>
              {date.num} {date.month}
            </p>
            <span className={styles.secondaryLine}></span>

            <p>{date.day}</p>
          </div>
        </div>
        <div className={styles.action}>
          <p className={styles.subText}>
            Five Space Square is in a certain sense a pragmatic systematization
            of the surrounding environment based on the difference between sand
            and stone, going from small to large, framed by pre-rusted corten
            steel. It is analytical in nature, as my work often is, and by
            decomposing the earth that is walked upon, it gives both
            understanding and outlook. It also deals with the switching of
            orientation; what is in nature vertically is presented horizontally,
            thereby creating a liminal space.
          </p>
          <button>buy now</button>
        </div>
      </section>
    </div>
  );
}
