import axios from 'axios';
import Grid from '../../shared/grid/Grid';
import { contact } from '../../assets/data';
import { API } from '../../config';

import styles from './Events.module.scss';

export async function getServerSideProps() {
  const res = await axios.get(`${API}/event`);
  let events = [];

  if (res.status === 200) {
    events = res.data?.data;
  }

  return {
    props: { events },
  };
}

export default function Events({ events }) {
  return (
    <div className={styles.eventPage}>
      <Grid eventPage={true} contact={contact} events={events} />
    </div>
  );
}
