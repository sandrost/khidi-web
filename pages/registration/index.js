import React from "react";
import FormLayout from "../../shared/layout/form/FormLayout";
import * as yup from "yup";

const fields = [
  {
    name: "name",
    type: "text",
    label: "Name",
  },
  {
    name: "surname",
    type: "text",
    label: "Surname",
  },
  {
    name: "username",
    type: "text",
    label: "Username",
  },
  {
    name: "email",
    type: "email",
    label: "Email",
  },
  {
    name: "resident",
    label: "Resident",
    type: "select",
    option: [
      {
        label: "Georgia",
        value: "georgia",
        name: "georgia",
      },
      {
        label: "Other",
        value: "other",
        name: "other",
      },
    ],
  },
  {
    name: "idnumber",
    type: "number",
    label: "ID Number",
  },
  {
    name: "social",
    type: "text",
    label: "Facebook id or instagram",
  },
  {
    name: "password",
    type: "password",
    label: "Password",
  },
  {
    name: "repeatpasssword",
    type: "password",
    label: "Repeat Passsword",
  },
];

const initialValues = {
  name: "",
  surname: "",
  username: "",
  email: "",
  resident: "",
  idnumber: "",
  socialid: "",
  password: "",
  repeatpasssword: "",
  check: "",
};
const schema = {
  name: yup.string("Enter yorr name").required("Name"),
  surname: yup.string("Enter yorr Surname").required("Surname"),
  username: yup.string("Enter yor Username").required("Username"),
  email: yup
    .string("Enter your email")
    .email("Enter a valid email")
    .required("Email"),
  idnumber: yup.string("Enter yor Id number"),
  password: yup.string("Enter your password").required("Password"),
  repeatpasssword: yup
    .string("Enter your password")
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Password"),
  check: yup.boolean().oneOf([true], "This field must be checked"),
};

const Registration = () => {
  return (
    <FormLayout
      fields={fields}
      initialValues={initialValues}
      schema={schema}
      submitText={"registration"}
      title={"registration"}
      style={"registration"}
      registration={true}
      show={true}
    ></FormLayout>
  );
};

export default Registration;
