import { React, useState } from "react";
import FormLayout from "../../shared/layout/form/FormLayout";
import * as yup from "yup";
import Succes from "./Succes";

const fields = [
  {
    name: "password",
    type: "password",
    label: "Password",
  },
  {
    name: "repeatpasssword",
    type: "password",
    label: "Repeat Passsword",
  },
];

const initialValues = {
  email: "",
  password: "",
  check: "",
};
const schema = {
  // password: yup
  //   .string("Enter your password")
  //   .min(8, "Password should be of minimum 8 characters length")
  //   .required("Password is required"),
  // repeatpasssword: yup
  //   .string("Enter your password")
  //   .oneOf([yup.ref("password"), null], "Passwords must match")
  //   .required("Password is required"),
};

const ChangePassword = () => {
  const [next, setNext] = useState(true);

  return (
    <>
      {next ? (
        <FormLayout
          fields={fields}
          initialValues={initialValues}
          schema={schema}
          submitText={"Send"}
          title={"Resset Password"}
          style={"resetPasswordnext"}
          show={true}
          Repeatpasssword={true}
          cusButtom={
            <button type="submit" onClick={() => setNext(!next)}>
              Next
            </button>
          }
        />
      ) : (
        <Succes />
      )}
    </>
  );
};

export default ChangePassword;
