import React from "react";
import FormLayout from "../../shared/layout/form/FormLayout";

const Succes = () => {
  return (
    <FormLayout
      submitText={"login"}
      title={"success page"}
      show={false}
    ></FormLayout>
  );
};

export default Succes;
