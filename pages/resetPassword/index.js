import { useState, React } from "react";
import FormLayout from "../../shared/layout/form/FormLayout";
import * as yup from "yup";
import ChangePassword from "./ChangePassword";

const fields = [
  {
    name: "email",
    type: "text",
    label: "Email",
  },
];

const initialValues = {
  email: "",
};
const schema = {
  // email: yup
  //   .string("Enter your email")
  //   .email("Enter a valid email")
  //   .required("Email is required"),
};

const Resetpassword = () => {
  const [next, setNext] = useState(true);

  return (
    <>
      {next ? (
        <FormLayout
          fields={fields}
          initialValues={initialValues}
          schema={schema}
          title={"Resset Password"}
          style={"resetpassword"}
          show={true}
          cusButtom={
            <button type="submit" onClick={() => setNext(!next)}>
              Next
            </button>
          }
        ></FormLayout>
      ) : (
        <ChangePassword />
      )}
    </>
  );
};

export default Resetpassword;
