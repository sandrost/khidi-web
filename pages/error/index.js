import styles from "./Error.module.scss";
import { React } from "react";
import notfound from "../../assets/404.png";
import discord from "../../assets/footer/discord.png";
import instagram from "../../assets/footer/instagram.png";
import telegram from "../../assets/footer/telegram.png";
import twitter from "../../assets/footer/twitter.png";

const Error = () => {
  return (
    <div className={styles.Error}>
      <div className={styles.errortitle}>
        <h2>404</h2>
        <p>not founD</p>
      </div>
      <div className={styles.middlecontent}>
        <img src={notfound.src}></img>
        <span>
          understanding and outlook. It also deals with the switching of
          orientation
        </span>
      </div>
      <div className={styles.contactinfo}>
        <div className={styles.header}>
          <span>
            Tbilisi <span>Georgia</span>
          </span>
          <p>vakhushti bagrationi bridge N 123</p>
        </div>
        <div className={styles.footer}>
          <div>
            <span className={styles.mail}>Info@khidi.com</span>
            <span className={styles.number}>+995 (95) 594523</span>
          </div>
          <ul>
            <li>
              <a href="#">
                <img src={twitter.src}></img>
              </a>
            </li>
            <li>
              <a href="#">
                <img src={telegram.src}></img>
              </a>
            </li>
            <li>
              <a href="#">
                <img src={instagram.src}></img>
              </a>
            </li>
            <li>
              <a href="#">
                <img src={discord.src}></img>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Error;
