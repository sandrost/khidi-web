
export const contact = [
    {
        id: "1",
        title: "Tbilisi",
        location: "vakhushti bagrationi bridge  N 123",
        number: "+995 (95) 594523",
        email: "Info@khidi.com",
        contact: true,
        getInTouch: true
    },
    {
        id: "2",
        title: "Resident",
        text: " Designed To Warm The Winter Season",
        subText: "I’m not shy about directing and editing an image, if it means I can more clearly convey an atmosphere or feeling",
        contact: true,
        resident: true


    },
    {
        id: "3",
        text: "Podcast Series",
        subText: "KHIDI podcast series covers a wide spectrum of electronic music with mixes of Georgian and international artists falling within the industrial, electro and techno genres. By balancing established and upcoming artists, podcasts offer opportunities for both prospective artists and industry legends to show their skills and to share music they love. With over 50 podcasts published, and two coming out each month, KHIDI lives up to its reputation for high quality electronic music and diverse program.",
        contact: true,
        label: true

    },



];

