import { React, useState } from "react";
import styles from "./FormLayout.module.scss";
import logo from "../../../assets/formlogo.png";
import infoicon from "../../../assets/i.svg";
import passicon from "../../../assets/password-icon.png";
import { Formik, Form, Field } from "formik";
import Error from "./ErorMessage/Error";
import * as yup from "yup";

const FormLayout = ({
  submitText,
  fields,
  initialValues,
  schema,
  title,
  style,
  registration,
  login,
  show,
  Repeatpasssword,
  cusButtom,
}) => {
  const SignupSchema = yup.object(schema);
  const [residentregion, setResidentregion] = useState("");

  console.log();

  return (
    <div className={styles.FormLayout}>
      <div className={styles.header}>
        <img className={styles.logo} src={logo.src}></img>
        <h2>KHIDI</h2>
        <h3>{title}</h3>
        <p>
          understanding and outlook. It also deals with the switching of
          orientation
        </p>
        <hr />
      </div>
      {show && (
        <Formik
          initialValues={initialValues}
          validationSchema={SignupSchema}
          onSubmit={(values) => {
            console.log(values);
          }}
        >
          {({ errors, touched }) => (
            <Form className={styles[style]}>
              <div className={styles.inputWrapper}>
                {fields.map((item, key) => (
                  <div key={key} className={styles.inputs}>
                    {item.type === "select" ? (
                      <>
                        <Field
                          component="select"
                          name={item.name}
                          value={item.option.value}
                          onChange={(e) => setResidentregion(e.target.value)}
                        >
                          <option>{item.label}</option>
                          {item.option.map((i, key) => (
                            <option key={key} value={i.value}>
                              {i.label}
                            </option>
                          ))}
                        </Field>
                      </>
                    ) : item.name === "social" ? (
                      <div className={styles.socialselect}>
                        <Field
                          name={item.name}
                          className={[styles.inputs]}
                          placeholder={item.label}
                          type={item.type}
                        />
                        <div className={styles.registrinfo}>
                          <a>
                            <img src={infoicon.src}></img>
                          </a>
                          <span className={styles.profilelink}>
                            Write Your Facebook Profile Link
                          </span>
                        </div>
                      </div>
                    ) : item.name === "idnumber" &&
                      residentregion === "other" ? (
                      <Field
                        name={item.name}
                        className={styles.inputs}
                        placeholder={"Passport Number"}
                        type={item.type}
                      />
                    ) : (
                      <Field
                        name={item.name}
                        className={styles.inputs}
                        placeholder={item.label}
                        type={item.type}
                      />
                    )}
                    {errors[item.name] &&
                    touched[item.name] &&
                    errors[item.name] ? (
                      <Error title={errors[item.name]}></Error>
                    ) : null}

                    {Repeatpasssword && <img src={passicon.src}></img>}
                  </div>
                ))}
              </div>
              <div>
                {registration && (
                  <div className={styles.registrterms}>
                    <div className={styles.checkbox}>
                      <input type="checkbox" />
                    </div>
                    <div>
                      <span>
                        To complete registration, you must read and agree to our
                      </span>
                      <span>
                        <a>terms and conditions</a>
                      </span>
                    </div>
                  </div>
                )}
                {login && (
                  <>
                    <div className={styles.loginterms}>
                      <div className={styles.rememberme}>
                        <input type="checkbox" />
                        <p>remember me</p>
                      </div>
                      <div>
                        <span>
                          <a href="/resetpassword">forget password ?</a>
                        </span>
                      </div>
                    </div>
                  </>
                )}
              </div>
              <div className={styles.footer}>
                {cusButtom ? (
                  cusButtom
                ) : (
                  <button className={styles.button} type="submit">
                    {submitText}
                  </button>
                )}

                <hr />
              </div>
            </Form>
          )}
        </Formik>
      )}
    </div>
  );
};

export default FormLayout;
