import React from "react";
import styles from "./Error.module.scss";

const Error = ({ title }) => {
  return (
    <div className={styles.error}>
      <span>incorect {title}</span>
    </div>
  );
};

export default Error;
