import styles from './Layout.module.scss';
import Header from '../header/Header';
import Footer from '../footer/Footer';
import { useState, useCallback, useEffect } from 'react';
import Menu from '../menu/Menu';
import { useSpring } from 'react-spring';
import { useRouter } from 'next/router';

const Layout = ({ children }) => {
  const [show, setShow] = useState(false);
  const router = useRouter();
  const callback = useCallback((show) => {
    setShow(show);
  }, []);

  useEffect(() => {
    if (show) {
      setShow(!show);
    }
  }, [router.asPath]);

  const { y } = useSpring({
    y: show ? 180 : 0,
  });
  const menuAppear = useSpring({
    transform: show ? 'translate3D(0,0,0)' : 'translate3D(0,0px,0)',
    opacity: show ? 1 : 0,
    config: { duration: '500' },
  });
  return (
    <>
      <div className={styles.container}>
        <Header parentCallback={callback} />
        <div
          style={{ display: show ? 'block' : 'none' }}
          className={styles.menuLayout}
        >
          {show && <Menu show={show} menuAppear={menuAppear} />}
        </div>
        <main
          style={{ opacity: show ? 0 : 1, display: show ? 'none' : 'flex' }}
          className={styles.main}
        >
          {children}
        </main>
        <Footer showMenuSocial={show} />
      </div>
    </>
  );
};

export default Layout;
