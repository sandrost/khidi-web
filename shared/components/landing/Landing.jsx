import Image from 'next/image';

import logo from '../../../assets/svg/logo.svg';
import styles from './Landing.module.scss';

export const Landing = () => {
  return (
    <div className={styles.landing}>
      <div className={styles.head}>
        <h3>Designed To Warm The Winter Season</h3>
        <p>
          I’m not shy about directing and editing an image, if it means I can more clearly
        </p>
      </div>
      <figure>
        <Image src={logo} alt="Khidi" layout="fill" />
      </figure>
    </div>
  );
}