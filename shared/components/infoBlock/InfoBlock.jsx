import styles from './InfoBlock.module.scss';

export const InfoBlock = () => {
  return (
    <div className={styles.infoBlock}>
      <h3>Designed To Warm The Winter Season</h3>
      <div className={styles.bottomWrap}>
        <div className={styles.textWrap}>
          <i></i>
          <p>
            I’m not shy about directing and editing an image, if it means I can
            more clearly convey an atmosphere or feeling
          </p>
        </div>
        <div className={styles.coutWrap}>
          <span className={styles.total}>9</span>
          <span className={styles.info}>Upcoming Events</span>
        </div>
      </div>
    </div>
  );
};
