import styles from './Resident.module.scss';
import resident from '../../../assets/resident/resident.jpg';
import seeall from '../../../assets/resident/seeall.jpg';
import logo from '../../../assets/khidilogo.png';

const Resident = ({ data }) => {
  let circledtext = 'Designed To Warm The Winter Season';

  circledtext = circledtext.split('').map((char, i) => (
    <span key={i} style={{ transform: `rotate(${i * 8}deg` }}>
      {' '}
      {char}{' '}
    </span>
  ));
  return (
    <section className={styles.resident}>
      {data.getInTouch && (
        <section className={styles.contact}>
          <section className={styles.contactHeader}>
            <h2>
              {data.title} <span>Georgia</span>
            </h2>
            <p>{data.location}</p>
          </section>
          <section className={styles.contactContent}>
            <p>{data.number}</p>
            <p className={styles.email}>{data.email}</p>
            <button>Get in Touch</button>
          </section>
        </section>
      )}
      {data.resident && (
        <div className={styles.first}>
          <h2 className={styles.title}>{data.title}</h2>
          <>
            <div className={styles.wrapper}>
              <div
                className={styles.photo}
                style={{ backgroundImage: `url("${resident.src}")` }}
              >
                <div
                  className={styles.seeall}
                  style={{ backgroundImage: `url("${seeall.src}")` }}
                >
                  <span>See all</span>
                  <div>{circledtext}</div>
                </div>
              </div>
            </div>
            <div className={styles.residentAbout}>
              <span>{data.text}</span>
              <p>{data.subText}</p>
            </div>
          </>
        </div>
      )}
      {data.label && (
        <section className={styles.second}>
          <section className={styles.residenthed}>
            <h2 className={styles.labelTitle}>{data.text}</h2>
            {data.label && (
              <span>
                K <br />H<br />I<br />D<br />I
              </span>
            )}
          </section>
          <div className={styles.residentfoot}>
            <a href="#">
              <img src={logo.src}></img>
            </a>
            <p className={styles.subText}>{data.subText} </p>
          </div>
          <div className={styles.circles}>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </section>
      )}
    </section>
  );
};

export default Resident;
