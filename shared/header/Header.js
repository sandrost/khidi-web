import { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import styles from './Header.module.scss';

const Header = ({ parentCallback }) => {
  const [show, setShow] = useState(false);

  const router = useRouter();

  useEffect(() => {
    if (show) {
      setShow(!show);
    }
  }, [router.asPath]);

  return (
    <header className={styles.header}>
      <div className={styles.logo}>
        <Link href="/">
          <a>
            <div className={styles.image}></div>
          </a>
        </Link>
      </div>
      <div className={styles.navWrapper}>
        <nav className={styles.nav}>
          <Link href="/login">
            <a>Login </a>
          </Link>

          <Link href="/registration">
            <a>Registration </a>
          </Link>

          <Link href="/cart">
            <a>Cart </a>
          </Link>
        </nav>
        <div
          id={'burger-wrapper'}
          className={show ? styles.closeBurger : styles.burger}
          onClick={() => {
            setShow((show) => !show);
            parentCallback(!show);
          }}
        >
          <div></div>
          <p>menu</p>
        </div>
      </div>
    </header>
  );
};

export default Header;
