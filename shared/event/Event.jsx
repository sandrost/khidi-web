import Image from 'next/image';
import Link from 'next/link';

import { imageUrl } from '../../helper';

import styles from './Event.module.scss';

export const Event = ({ data }) => {
  console.log(styles);

  return (
    <div className={styles.event}>
      <div className={styles.top}>
        {data.image && (
          <figure>
            <Link href={`/event/${data.id}`}>
              <Image
                src={imageUrl(data.image)}
                alt={data.title_en}
                layout="fill"
              />
            </Link>
          </figure>
        )}
        <h3 className={styles.title}>
          <Link href={`/event/${data.id}`}>{data.title_en}</Link>
        </h3>
        <span className={styles.category}>{data.category?.title_en}</span>
      </div>
      <p className={styles.info}>{data.info_en}</p>
    </div>
  );
};
