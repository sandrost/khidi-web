import { Mousewheel } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import 'swiper/css';
import styles from './Grid.module.scss';

export const Grid = ({ data = [] }) => {
  return (
    <Swiper
      direction="horizontal"
      slidesPerView="auto"
      centeredSlides={false}
      mousewheel={true}
      modules={[Mousewheel]}
      allowTouchMove={true}
      noSwiping={false}
      className={styles.slider}
      followFinger={false}
      watchSlidesProgress={true}
      parallax={true}
    >
      {data.map((item, key) => {
        const Component = () => item.component;

        return (
          <SwiperSlide
            key={key}
            className={[styles[`size-${item.size}`], styles.slide].join(' ')}
          >
            <Component />
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};
