import styles from './Menu.module.scss';
import { animated } from 'react-spring';
import Link from 'next/link';

const Menu = ({ show, menuAppear }) => {
  return (
    <div className={styles.menu}>
      <div className={styles.wrap}>
        <animated.div style={menuAppear} className={styles.list}>
          <div className={styles.listWrapper}>
            <div className={styles.link}>
              <Link href="/">home</Link>
              <Link href="/events">program</Link>
              <a href="#">residents</a>
              <a href="#">agency</a>
              <a href="#">label</a>
            </div>
            <div className={styles.link}>
              <a
                href="https://khidinft.com/"
                target="_blank"
                rel="noopener noreferrer"
                className={styles.nft}
              >
                khidi nft
                <div className={styles.arrow}></div>
              </a>
              <a href="#">podcasts</a>
              <a href="#">art space</a>
              <a href="#">blog</a>
              <Link href="/contact">contact</Link>
            </div>
          </div>
          <div className={styles.contactText}>
            <hr />
            <p>
              {' '}
              understanding and outlook. It also deals with the switching of
              orientation; what is in nature vertically is presented
              horizontally, thereby creating a liminal space.
            </p>
          </div>
        </animated.div>
      </div>
      <div className={styles.wrap}>
        <animated.div style={menuAppear}>
          <div>
            <div className={styles.contactLogo}>
              <div></div>
              <p>khidi</p>
            </div>
            <div className={styles.register}>
              <h2>registration</h2>
              <p>
                understanding and outlook. It also deals with the switching of
                orientation
              </p>
              <hr />
            </div>
            <div className={styles.sign}>
              <button>sign in</button>
              <button>sign up</button>
            </div>
          </div>
        </animated.div>
      </div>
    </div>
  );
};

export default Menu;
