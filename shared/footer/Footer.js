import styles from './Footer.module.scss'
const Footer = ({ showMenuSocial }) => {
    return (
        <footer className={showMenuSocial ? styles.menuVisible : styles.footer}>
            <div className={styles.element}>
            </div>
            <div className={styles.element}>
                {

                    showMenuSocial &&
                    <div className={styles.wrapper}>
                        <ul>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                }

            </div>
        </footer >
    )
}

export default Footer;
