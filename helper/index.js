import { STORAGE } from '../config';

export const imageUrl = (src) => `${STORAGE}${src}`;
